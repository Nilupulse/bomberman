﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

	//Player Variables
	public float moveSpeed = 5f;
	public CurrentPlayer currentPlayer;
	public enum CurrentPlayer {Player1,Player2};//Select Player from the inspecter[Drop down]
	bool IsDead;
	//Prefabs
	public GameObject Bomb;
	//Components
	private Rigidbody playerRigidBody;
	private Transform playerTransform;

	void Start ()
	{
		playerRigidBody = GetComponent<Rigidbody>();
		playerTransform = transform;
	}

	void Update ()
	{
		movePlayer ();//Player Movements
	}

	void movePlayer()
	{
		//Movement for Player1
		if(currentPlayer==CurrentPlayer.Player1)
		{
			if (Input.GetKey(KeyCode.UpArrow))
			{
				playerRigidBody.velocity = new Vector3(playerRigidBody.velocity.x, playerRigidBody.velocity.y, moveSpeed);
			}

			if (Input.GetKey(KeyCode.LeftArrow))
			{
				playerRigidBody.velocity = new Vector3(-moveSpeed, playerRigidBody.velocity.y, playerRigidBody.velocity.z);
			}

			if (Input.GetKey(KeyCode.DownArrow))
			{ 
				playerRigidBody.velocity = new Vector3(playerRigidBody.velocity.x, playerRigidBody.velocity.y, -moveSpeed);
			}

			if (Input.GetKey(KeyCode.RightArrow)) 
			{
				playerRigidBody.velocity = new Vector3(moveSpeed, playerRigidBody.velocity.y, playerRigidBody.velocity.z);
			}
			//Plyer 1 Drop A Bomb
			if (Input.GetKeyDown(KeyCode.Space))
			{ 
				DropaBomb();
			}

		}
		//Movement for Player2
		else
		{
			if (Input.GetKey(KeyCode.W)) 
			{ 
				playerRigidBody.velocity = new Vector3(playerRigidBody.velocity.x, playerRigidBody.velocity.y, moveSpeed);
			}

			if (Input.GetKey(KeyCode.A)) 
			{ 
				playerRigidBody.velocity = new Vector3(-moveSpeed, playerRigidBody.velocity.y, playerRigidBody.velocity.z);
			}

			if (Input.GetKey(KeyCode.S))
			{ 
				playerRigidBody.velocity = new Vector3(playerRigidBody.velocity.x, playerRigidBody.velocity.y, -moveSpeed);
			}

			if (Input.GetKey(KeyCode.D))
			{ 
				playerRigidBody.velocity = new Vector3(moveSpeed, playerRigidBody.velocity.y, playerRigidBody.velocity.z);
			}
			//Plyer 2 Drop A Bomb
			if (Input.GetKeyDown(KeyCode.G))
			{ 
				DropaBomb();
			}
		}
	}
	//Drop a Bomb
	void DropaBomb()
	{
		Instantiate(Bomb,new Vector3(
			Mathf.RoundToInt(playerTransform.position.x),//Align the bomb in x axis
			Bomb.transform.position.y,
			Mathf.RoundToInt(playerTransform.position.z)),//Align the bomb in z axis
			Quaternion.identity);
	}
}

