﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameOver : MonoBehaviour {
	//Gameover Variables
	public Text winnerText;
	public GameObject gameOverPanel;
	public GameObject Player1;
	public GameObject Player2;
	public GameObject[] SpawnPoints;
	int spawnpoint;
	bool[] CanUseSpawnpoint = new bool[6];

	public static GameOver Instance;

	void Awake()
	{
		if(!Instance)
		Instance = this;
	}
	//Show GameOver Screen
	public void ShowGameOverScreen()
	{
		winnerText.text = GameManager.Winner;
		gameOverPanel.SetActive (true);
	}
	//Play Again
	public void TryAgain()
	{
		winnerText.text = "";
		GameManager.Plyer1Dead = false;
		GameManager.Plyer2Dead = false;
		gameOverPanel.SetActive (false);
		PlacePlayers ();
	}
	//Place Players Randomly
	void PlacePlayers()
	{
		spawnpoint=Random.Range (0, SpawnPoints.Length);
		Player1.transform.position=SpawnPoints[spawnpoint].transform.position;
		CanUseSpawnpoint [spawnpoint]=false;
		spawnpoint=Random.Range (0, SpawnPoints.Length);
		if (CanUseSpawnpoint [spawnpoint])
		{
			Player2.transform.position=SpawnPoints[spawnpoint].transform.position;
		}
		else
		{
			ResetPos ();
			Invoke ("PlacePlayers",0f);
		}
		Player1.SetActive (true);
		Player2.SetActive (true);

	}
	void ResetPos()
	{
		for(int i=0;i<CanUseSpawnpoint.Length;i++)
		{
			CanUseSpawnpoint [i] = true;
		}
	}
}
