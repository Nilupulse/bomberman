﻿using UnityEngine;
using System.Collections;

public class Bomb : MonoBehaviour
{
	//Bomb Variables
	[Range (2,5)]
	public int ExplotionRange;// select thr explotion range from the Inspecter [Slider]
	public LayerMask ObjectType; 
	//Prefabs
	public GameObject Explotion;

	void Start ()
	{		
		StartCoroutine (StartExplotion());
	}
	IEnumerator StartExplotion()
	{
		yield return new WaitForSeconds (3);
		Explode ();
	}
	void Explode()
	{
		//Explotion bomb posision
		GameObject _Explotion=(GameObject)Instantiate (Explotion,transform.position,Quaternion.identity);
		//Explotion for directions
		StartCoroutine(Explosions(Vector3.forward));
		StartCoroutine(Explosions(Vector3.right));
		StartCoroutine(Explosions(Vector3.back));
		StartCoroutine(Explosions(Vector3.left));

		Destroy(gameObject,.3f);
		Destroy(_Explotion,.3f);

	}
	private IEnumerator Explosions(Vector3 direction)
	{
		GameObject _Explotion;
		for (int StartPos = 1; StartPos <ExplotionRange; StartPos++)
		{ 
			RaycastHit hit;

			Physics.Raycast(transform.position + new Vector3(0,.5f,0), direction, out hit, StartPos, ObjectType);
			if (!hit.collider) 
			{				
				_Explotion=(GameObject)Instantiate(Explotion, transform.position + (StartPos * direction), Explotion.transform.rotation);
			}
			else if(hit.transform.gameObject.tag =="Distractable")
			{
				_Explotion=(GameObject)Instantiate(Explotion, transform.position + (StartPos * direction), Explotion.transform.rotation);
				Destroy (hit.transform.gameObject);
			}
			else if(hit.transform.gameObject.tag =="Player")
			{
				_Explotion=(GameObject)Instantiate(Explotion, transform.position + (StartPos * direction), Explotion.transform.rotation);
				if (hit.transform.gameObject.GetComponent<Player> ().currentPlayer == Player.CurrentPlayer.Player1) 
				{
					//Debug.Log ("1 kill");
					GameManager.Plyer1Dead = true;
				} 
				else if (hit.transform.gameObject.GetComponent<Player> ().currentPlayer == Player.CurrentPlayer.Player2)
				{
					//Debug.Log ("2 kill");
					GameManager.Plyer2Dead = true;
				}
				Invoke("ChickWinner",.3f);
				hit.transform.gameObject.SetActive(false);
			}
			else
			{
				break;
			}

			Destroy(_Explotion,.3f);
			yield return new WaitForSeconds(.05f);

		}
	}
	// Check the Winner
	void ChickWinner()
	{
		if(GameManager.Plyer1Dead && !GameManager.Plyer2Dead)
		{
			Debug.Log ("Player 1 dead");
			GameManager.Winner="Player 1 Dead,Winner Player 2";
		}
		else if(GameManager.Plyer2Dead && !GameManager.Plyer1Dead)
		{
			Debug.Log ("Player 2 dead");
			GameManager.Winner="Player 2 Dead,Winner Player 1";
		}
		else if(GameManager.Plyer2Dead && GameManager.Plyer1Dead)
		{
			Debug.Log ("Draw");
			GameManager.Winner="It's a Draw";
		}
		GameOver.Instance.ShowGameOverScreen ();
	}
}
